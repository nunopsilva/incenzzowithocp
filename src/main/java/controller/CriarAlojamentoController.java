package controller;

import domain.Organizacao;
import domain.TipoAlojamento;
import domain.Alojamento.DiaSemana;

import java.util.List;

import domain.Alojamento;

public class CriarAlojamentoController {

    private Organizacao m_oOrganizacao;
    private Alojamento m_oAlojamento;

    public CriarAlojamentoController(Organizacao oOrganizacao) {
             this.m_oOrganizacao=oOrganizacao;
    }

    public boolean criaAlojamento(String sDenominacao, TipoAlojamento oTipoAlojamento, DiaSemana diaSemana, int nQtdMinPax, int nQtdMaxPax, double fPreco )
    {
        try {
            m_oAlojamento = m_oOrganizacao.novoAlojamento(sDenominacao, oTipoAlojamento, diaSemana, nQtdMinPax, nQtdMaxPax, fPreco);
         
            if( m_oOrganizacao.validaAlojamento(m_oAlojamento) )
                return true;
            else
                return false;
        }
        catch( IllegalArgumentException exception ) {
            return false;
        }
    }

    public boolean guardaAlojamento()
    {
        return this.m_oOrganizacao.guardaAlojamento(m_oAlojamento);
    }

    public String getAlojamentoString()
    {

        return this.m_oAlojamento.toString();
    }

    public List<TipoAlojamento> getTipoAlojamentos() {
        return m_oOrganizacao.getTipoAlojamentos();
    }
}

package controller;

import domain.Organizacao;
import domain.TipoAlojamento;
import ui.controllersinterfaces.CriarTipoAlojamentoController;

public class CriarTipoAlojamentoController1 implements CriarTipoAlojamentoController {

    private Organizacao m_oOrganizacao;
    private TipoAlojamento m_oTipoAlojamento;

    public CriarTipoAlojamentoController1(Organizacao oOrganizacao) {

        if( oOrganizacao != null )
             this.m_oOrganizacao=oOrganizacao;
        else
            throw new IllegalArgumentException("Organizacao não pode ser null.");
    }

    public boolean criaTipoAlojamento(String desc)
    {
        try {
            m_oTipoAlojamento = m_oOrganizacao.novoTipoAlojamento(desc);

            if( m_oOrganizacao.validaTipoAlojamento(m_oTipoAlojamento) )
                return true;
            else
                return false;
        }
        catch( IllegalArgumentException exception ) {
            return false;
        }
    }

    public boolean guardaTipoAlojamento()
    {
        return this.m_oOrganizacao.guardaTipoAlojamento(m_oTipoAlojamento);
    }

    public String getTipoAlojamentoString()
    {
        return this.m_oTipoAlojamento.toString();
    }
}

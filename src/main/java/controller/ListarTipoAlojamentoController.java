package controller;

import java.util.List;

import domain.Organizacao;
import domain.TipoAlojamento;
import domain.filters.TipoAlojamentoFilter;

public class ListarTipoAlojamentoController {

    private Organizacao m_oOrganizacao;

    public ListarTipoAlojamentoController(Organizacao oOrganizacao) {

        if( oOrganizacao != null )
             this.m_oOrganizacao=oOrganizacao;
        else
            throw new IllegalArgumentException("Organização não pode ser null.");
    }

    public List<TipoAlojamentoFilter> getTipoAlojamentoFilters() {
        return this.m_oOrganizacao.getTipoAlojamentoFilters();
    }

    public List<TipoAlojamento> filtrar( TipoAlojamentoFilter filter, String string) {
        List<TipoAlojamento> lista = this.m_oOrganizacao.filtrarTiposAlojamento( filter, string );

        return lista;
    }
}

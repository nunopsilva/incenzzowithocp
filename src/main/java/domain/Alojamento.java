package domain;

public class Alojamento {

    public enum DiaSemana {DOMINGO, SEGUNDA_FEIRA, TERÇA_FEIRA, QUARTA_FEIRA, QUINTA_FEIRA, SEXTA_FEIRA, SÁBADO }; 

    String m_sDenominacao;
    private TipoAlojamento m_oTipoAlojamento;
    int m_nQtdMinPax = 0;
    int m_nQtdMaxPax = 0;
    DiaSemana m_diaSemana;
    double m_fPreco = 0;

    
    public Alojamento(String sDenominacao, TipoAlojamento oTipoAlojamento, DiaSemana diaSemana, int nQtdMinPax, int nQtdMaxPax, double fPreco ) {

        if( sDenominacao != null && !sDenominacao.isBlank() && !sDenominacao.isEmpty() )
            this.m_sDenominacao = sDenominacao;
        else
            throw new IllegalArgumentException("Valor de parâmetro 'descrição' deve ser uma string não vazia.");


        if( oTipoAlojamento != null )
            m_oTipoAlojamento = oTipoAlojamento;
        else
            throw new IllegalArgumentException("Valor de parâmetro 'tipo de alojamento' deve ser um objeto de 'TipoAlojamento'.");


        if( nQtdMinPax < 1 && nQtdMinPax > nQtdMaxPax )
            throw new IllegalArgumentException("Valor de parâmetro 'quantidade mínima pax' deve ser maior que 0 e menor ou igual a 'quantidade máxima pax'.");

        m_nQtdMinPax = nQtdMinPax;
        m_nQtdMaxPax = nQtdMaxPax;

        m_diaSemana = diaSemana;

        if( fPreco > 0 )
            m_fPreco = fPreco;
        else
            throw new IllegalArgumentException("Valor de parâmetro 'preço' deve ser maior que 0.");
    }

    public boolean equals( Object object ) {

        if( this == object )
            return true;

        if( object instanceof Alojamento ) {
            Alojamento oAlojamento = (Alojamento) object;

            if( this.m_sDenominacao.equals( oAlojamento.m_sDenominacao) &&
                this.m_diaSemana == oAlojamento.m_diaSemana &&
                this.m_oTipoAlojamento.equals(oAlojamento.m_oTipoAlojamento) &&
                this.m_nQtdMinPax == oAlojamento.m_nQtdMinPax &&
                this.m_nQtdMaxPax == oAlojamento.m_nQtdMaxPax
              )
                return true;
        }
        return false;
    }

    @Override
    public String toString() {
        return "\nDenominação: " + m_sDenominacao + "\nTipo Alojamento: \n\t" + m_oTipoAlojamento.toString() + "\nDia Semana: " + m_diaSemana.toString() + "\nQuantidade mínima pax = " + this.m_nQtdMinPax + "\nQuantidade máxima pax = " + this.m_nQtdMaxPax + "\nPreço: " + this.m_fPreco;
    }

    public String getDenominacao() {
        return this.m_sDenominacao;
    }

    public int getMinQtd() {
        return m_nQtdMinPax;
    }

    public int getMaxQtd() {
        return m_nQtdMaxPax;
    }
}

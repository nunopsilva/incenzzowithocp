package domain;

import domain.Alojamento.DiaSemana;

public interface FactoryAlojamento {

    public Alojamento criaAlojamento( String sDenominacao, TipoAlojamento oTipoAlojamento, DiaSemana diaSemana,
                                      int nQtdMinPax, int nQtdMaxPax, double fPreco );
}

package domain;

public interface FactoryTipoAlojamento {

    public TipoAlojamento criarTipoAlojamento( String sDenominacao );
    
}

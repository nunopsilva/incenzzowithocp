package domain;

public class ImplFactoryTipoAlojamento implements FactoryTipoAlojamento{

    public TipoAlojamento criarTipoAlojamento( String sDenominacao ) {
        
        return new TipoAlojamento( sDenominacao );
    }
}

package domain;


import java.util.ArrayList;
import java.util.List;

import domain.Alojamento.DiaSemana;
import domain.filters.TipoAlojamentoFilter;
//import domain.filters.AlojamentoFilterByDenominacao;
// import domain.filters.IAlojamentoFilterByIntervaloQtd;

public class Organizacao {

    FactoryTipoAlojamento m_oFactoryTipoAlojamento;
    FactoryAlojamento m_oFactoryAlojamento;

    private final List<TipoAlojamento> m_lstListaTiposAlojamento = new ArrayList<TipoAlojamento>();
    private final List<Alojamento> m_lstListaAlojamento = new ArrayList<Alojamento>();

    private final List<TipoAlojamentoFilter> m_listaTipoAlojamentoFilters;
    //private final List<AlojamentoFilterByDenominacao> m_listStringClassesAlojamentoFiltersByDenominacao;
    //private final List<IAlojamentoFilterByIntervaloQtd> m_listStringClassesAlojamentoFiltersByIntervaloQtd;

    public Organizacao( FactoryTipoAlojamento oFactoryTipoAlojamento, FactoryAlojamento oFactoryAlojamento,
                                        List<String> listaStringClassesTipoAlojamentoFilters,
                                        List<String> listStringClassesAlojamentoFiltersByDenominacao, 
                                        List<String> listStringClassesAlojamentoFiltersByIntervaloQtd )
    {
        if( oFactoryTipoAlojamento != null )
            this.m_oFactoryTipoAlojamento = oFactoryTipoAlojamento;
        else
            throw new IllegalArgumentException("FactoryTipoAlojamento não pode ser null.");

        if( oFactoryAlojamento != null )
            this.m_oFactoryAlojamento = oFactoryAlojamento;
        else
            throw new IllegalArgumentException("FactoryAlojamento não pode ser null.");

        if( listaStringClassesTipoAlojamentoFilters != null )
            this.m_listaTipoAlojamentoFilters = createInstancesOfTipoAlojamentoFilters( listaStringClassesTipoAlojamentoFilters );
        else
            throw new IllegalArgumentException("TipoAlojamentoFilters não pode ser null.");

            /*
        if( listStringClassesAlojamentoFiltersByDenominacao != null )
            this.m_listStringClassesAlojamentoFiltersByDenominacao = createInstancesOfAlojamentoFiltersByDenominacao( listStringClassesAlojamentoFiltersByDenominacao );
        else
            throw new IllegalArgumentException("AlojamentoFiltersByDenominacao não pode ser null.");

        if( listStringClassesAlojamentoFiltersByIntervaloQtd != null )
            this.m_listStringClassesAlojamentoFiltersByIntervaloQtd = createInstancesOfAlojamentoFiltersByIntervaloQtd( listStringClassesAlojamentoFiltersByIntervaloQtd );
        else
            throw new IllegalArgumentException("AlojamentoFiltersByIntervaloQtd não pode ser null.");
*/
    }

    private List<TipoAlojamentoFilter> createInstancesOfTipoAlojamentoFilters( List<String> listaStringClassesTipoAlojamentoFilters ) {

        List<TipoAlojamentoFilter> lTipoAlojamentoFilters = new ArrayList<TipoAlojamentoFilter>();

        for (String strClassTipoAlojamentoFilter : listaStringClassesTipoAlojamentoFilters) {
            try {
                TipoAlojamentoFilter oTipoAlojamentoFilter = (TipoAlojamentoFilter) Class.forName(strClassTipoAlojamentoFilter).getDeclaredConstructor().newInstance();
                lTipoAlojamentoFilters.add(oTipoAlojamentoFilter);
            }
            catch(Exception e) {
                System.out.println( e.getMessage() );
            }
        }

        return lTipoAlojamentoFilters;
    }
/*
    private List<AlojamentoFilterByDenominacao> createInstancesOfAlojamentoFiltersByDenominacao( List<String> listaStringClassesTipoAlojamentoFilters ) {

        List<AlojamentoFilterByDenominacao> lAlojamentoFiltersByDenominacao = new ArrayList<AlojamentoFilterByDenominacao>();

        for (String strClassTipoAlojamentoFilter : listaStringClassesTipoAlojamentoFilters) {
            try {
                AlojamentoFilterByDenominacao oTipoAlojamentoFilter = (AlojamentoFilterByDenominacao) Class.forName(strClassTipoAlojamentoFilter).getDeclaredConstructor().newInstance();
                lAlojamentoFiltersByDenominacao.add(oTipoAlojamentoFilter);
            }
            catch(Exception e) {
                System.out.println( e.getMessage() );
            }
        }

        return lAlojamentoFiltersByDenominacao;
    }

    private List<IAlojamentoFilterByIntervaloQtd> createInstancesOfAlojamentoFiltersByIntervaloQtd( List<String> listStringClassesAlojamentoFiltersByIntervaloQtd ) {

        List<IAlojamentoFilterByIntervaloQtd> lAlojamentoFiltersByDenominacao = new ArrayList<IAlojamentoFilterByIntervaloQtd>();

        for (String strClassTipoAlojamentoFilter : listStringClassesAlojamentoFiltersByIntervaloQtd) {
            try {
                IAlojamentoFilterByIntervaloQtd oTipoAlojamentoFilter = (IAlojamentoFilterByIntervaloQtd) Class.forName(strClassTipoAlojamentoFilter).getDeclaredConstructor().newInstance();
                lAlojamentoFiltersByDenominacao.add(oTipoAlojamentoFilter);
            }
            catch(Exception e) {
                System.out.println( e.getMessage() );
            }
        }

        return lAlojamentoFiltersByDenominacao;
    }
*/
    /*
    // antes de reengenharia
    public TipoAlojamento novoTipoAlojamento(String desc)
    {
        return (new TipoAlojamento(desc));
    }
    */

    /*
    // depois de reengenharia
    */
    public TipoAlojamento novoTipoAlojamento(String desc)
    {
        return this.m_oFactoryTipoAlojamento.criarTipoAlojamento( desc );
    }

    public boolean validaTipoAlojamento(TipoAlojamento ta)
    {
        if( ta != null )
            return true;
        else
            return false;
    }

    public boolean guardaTipoAlojamento(TipoAlojamento ta)
    {
        if (this.validaTipoAlojamento(ta))
        {
            return m_lstListaTiposAlojamento.add(ta);
        }
        return false;
    }

    /*
    // antes de reengenharia
    public Alojamento novoAlojamento(String desc, TipoAlojamento oTipoAlojamento, DiaSemana diaSemana, int nQtdMinPax, int nQtdMaxPax, double fPreco )
    {
        return new Alojamento(desc, oTipoAlojamento, diaSemana, nQtdMinPax, nQtdMaxPax, fPreco);
    }
    */

    /*
    // depois de reengenharia
    */
    public Alojamento novoAlojamento(String desc, TipoAlojamento oTipoAlojamento, DiaSemana diaSemana, int nQtdMinPax, int nQtdMaxPax, double fPreco )
    {
        return this.m_oFactoryAlojamento.criaAlojamento(desc, oTipoAlojamento, diaSemana, nQtdMinPax, nQtdMaxPax, fPreco);
    }

    public boolean validaAlojamento(Alojamento oAlojamento )
    {
        if( oAlojamento != null )
            return true;
        else
            return false;
    }

    public boolean guardaAlojamento(Alojamento oAlojamento)
    {
        if (this.validaAlojamento(oAlojamento))
        {
            return m_lstListaAlojamento.add(oAlojamento);
        }
        return false;
    }

    public List<TipoAlojamento> getTipoAlojamentos() {

        List<TipoAlojamento> lstTipoAlojamentos = new ArrayList<TipoAlojamento>();

        lstTipoAlojamentos.addAll( this.m_lstListaTiposAlojamento );

        return lstTipoAlojamentos;
    }


    public List<TipoAlojamentoFilter> getTipoAlojamentoFilters() {
        return new ArrayList<>(this.m_listaTipoAlojamentoFilters);
    }

    /*
    public List<AlojamentoFilterByDenominacao> getAlojamentoFiltersByDenominacao() {
        return new ArrayList<>(this.m_listStringClassesAlojamentoFiltersByDenominacao);
    }

    public List<IAlojamentoFilterByIntervaloQtd> getAlojamentoFiltersByIntervaloQtds() {
        return new ArrayList<>(this.m_listStringClassesAlojamentoFiltersByIntervaloQtd);
    }
*/
    public List<TipoAlojamento> filtrarTiposAlojamento( TipoAlojamentoFilter filtro, String string ) {

        List<TipoAlojamento> listaFiltrada = new ArrayList<TipoAlojamento>();

        for (TipoAlojamento oTipoAlojamento : m_lstListaTiposAlojamento) {

            if( filtro.complies( oTipoAlojamento, string ) )
                listaFiltrada.add( oTipoAlojamento );
        }
        return listaFiltrada;
    }

    /*
    public List<Alojamento> filtrarAlojamentosByDenominacao( AlojamentoFilterByDenominacao filtro, String string ) {

        List<Alojamento> listaFiltrada = new ArrayList<Alojamento>();

        for (Alojamento oAlojamento : m_lstListaAlojamento) {

            if( filtro.complies( oAlojamento, string ) )
                listaFiltrada.add( oAlojamento );
        }
        return listaFiltrada;
    }

    public List<Alojamento> filtrarAlojamentosByItervaloQtd( IAlojamentoFilterByIntervaloQtd filtro, int nQtdMinPax, int nQtdMaxPax ) {

        List<Alojamento> listaFiltrada = new ArrayList<Alojamento>();

        for (Alojamento oAlojamento : m_lstListaAlojamento) {

            if( filtro.complies( oAlojamento, nQtdMinPax, nQtdMaxPax ) )
                listaFiltrada.add( oAlojamento );
        }
        return listaFiltrada;
    }
    */
}

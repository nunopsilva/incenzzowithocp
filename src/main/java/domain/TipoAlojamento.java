package domain;

public class TipoAlojamento {

    private String m_sDescricao;

    public TipoAlojamento(String sDescricao) {

        m_sDescricao=sDescricao;

        if( sDescricao != null && !sDescricao.isBlank() && !sDescricao.isEmpty() )
            this.m_sDescricao = sDescricao;
        else
            throw new IllegalArgumentException("Valor de parâmetro 'descrição' deve ser uma string não vazia.");
    }

    public boolean equals( Object object ) {

        if( this == object )
            return true;

        if( object instanceof TipoAlojamento ) {
            TipoAlojamento tipoAlojamento = (TipoAlojamento) object;

            if( this.m_sDescricao.equals( tipoAlojamento.m_sDescricao ) )
                return true;
        }
        return false;
    }

    @Override
    public String toString() {
        return "Denominação: "+ m_sDescricao;
    }

    public String getDescricao() {
        return m_sDescricao;
    }
}

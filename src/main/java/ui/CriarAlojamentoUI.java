package ui;

import java.util.ArrayList;
import java.util.Arrays;

import controller.CriarAlojamentoController;
import domain.Alojamento;
import domain.Organizacao;
import domain.TipoAlojamento;
import domain.Alojamento.DiaSemana;
import utils.Utils;

public class CriarAlojamentoUI {

    private Organizacao m_oOrganizacao;
    private CriarAlojamentoController m_controller;

    public CriarAlojamentoUI(Organizacao oOrganizacao)
    {
        this.m_oOrganizacao = oOrganizacao;
        m_controller = new CriarAlojamentoController(m_oOrganizacao);
    }

    public void run()
    {
        System.out.println("\nNovo Alojamento:");
        introduzDados();

        apresentaAlojamento();

        if (Utils.confirma("Confirma os dados do Alojamento? (S/N)")) {
            if (m_controller.guardaAlojamento()) {
                System.out.println("Alojamento guardado.");
            } else {
                System.out.println("Alojamento não guardado.");
            }
        }
    }

    private void introduzDados() {
        String sDescricao = Utils.readLineFromConsole("Descrição: ");        
        
        DiaSemana diaSemana = readDiaSemana();

        TipoAlojamento oTipoAlojamento = readTipoAlojamento();

        String sQtdMinPax = Utils.readLineFromConsole("Quantidade mínima de pessoas: ");
        int nQtdMinPax = Integer.parseInt( sQtdMinPax );

        String sQtdMaxPax = Utils.readLineFromConsole("Quantidade máxima de pessoas: ");
        int nQtdMaxPax = Integer.parseInt( sQtdMaxPax );

        String sPreco = Utils.readLineFromConsole("Preço: ");
        double fPreco = Double.parseDouble( sPreco );

        m_controller.criaAlojamento(sDescricao, oTipoAlojamento, diaSemana, nQtdMinPax, nQtdMaxPax, fPreco);
    }

    private TipoAlojamento readTipoAlojamento() 
    {
        ArrayList<TipoAlojamento> lstTipoAlojamentos = new ArrayList<>( m_controller.getTipoAlojamentos() );

        int nIndex = 0;
        do {
            int i = 0;
            for (TipoAlojamento tipoAlojamento : lstTipoAlojamentos) {
                i++;
                System.out.println(i + ". " + tipoAlojamento.toString() );
            }

            String sTipoAlojamento = Utils.readLineFromConsole("Tipo Alojamento: ");

            nIndex = Integer.parseInt(sTipoAlojamento );

            if( nIndex <= lstTipoAlojamentos.size() )
                return lstTipoAlojamentos.get( nIndex - 1 );
        }
        while ( nIndex !=0 );

        return null;
    }

    private DiaSemana readDiaSemana()
    {
        ArrayList<DiaSemana> diasSemana = new ArrayList<DiaSemana>( Arrays.asList( Alojamento.DiaSemana.values() ) );

        int nIndex = 0;
        do {
            int i = 0;
            for ( DiaSemana day :  diasSemana ) {
                i++;
                System.out.println( i + ". " + day );
            }
            
            String sDiaSemana = Utils.readLineFromConsole("Dia Semana: ");

            nIndex = Integer.parseInt(sDiaSemana );

            if( nIndex <= diasSemana.size() )
                return diasSemana.get( nIndex - 1 );
        }
        while ( nIndex !=0 );

        return null;
    }

    private void apresentaAlojamento()
    {
        System.out.println("\nAlojamento:\n" + m_controller.getAlojamentoString());
    }
}

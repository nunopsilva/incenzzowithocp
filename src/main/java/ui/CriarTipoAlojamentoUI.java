package ui;

import ui.controllersinterfaces.CriarTipoAlojamentoController;
import domain.Organizacao;
import utils.Utils;

public class CriarTipoAlojamentoUI implements UI {

    private Organizacao m_oOrganizacao;
    private CriarTipoAlojamentoController m_controller;

    public CriarTipoAlojamentoUI( Organizacao oOrganizacao ) throws Exception
    {
        this.m_oOrganizacao = oOrganizacao;
        
        // m_controller = new CriarTipoAlojamentoController1(m_oOrganizacao);
        m_controller = criaInstaciaDeController();
    }

    private CriarTipoAlojamentoController criaInstaciaDeController() throws Exception {

        String strClassController = Utils.readConfigString("controllers.CriarTipoAlojamentoController");

        CriarTipoAlojamentoController controller = (CriarTipoAlojamentoController)
                                Class.forName(strClassController)
                                .getDeclaredConstructor(Organizacao.class)
                                .newInstance(this.m_oOrganizacao);

        return controller;
    }

    public String getMenuDescription() {
        return "Criar Tipo Alojamento";
    }

    public void run(  )
    {
        System.out.println("\nNovo Tipo de Alojamento:");
        introduzDados();

        apresentaTipoAlojamento();

        if (Utils.confirma("Confirma os dados do Tipo Alojamento? (S/N)")) {
            if (m_controller.guardaTipoAlojamento()) {
                System.out.println("Tipo de Alojamento guardado.");
            } else {
                System.out.println("Tipo de Alojamento não guardado.");
            }
        }
    }

    private void introduzDados() {
        String sDescricao = Utils.readLineFromConsole("Introduza Descricão: ");

        m_controller.criaTipoAlojamento(sDescricao);
    }

    private void apresentaTipoAlojamento()
    {
        System.out.println("\nTipo de Alojamento:\n" + m_controller.getTipoAlojamentoString());
    }
}

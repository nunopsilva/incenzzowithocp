package ui;

import java.util.ArrayList;
import java.util.List;

import controller.ListarTipoAlojamentoController;
import domain.Organizacao;
import domain.TipoAlojamento;
import domain.filters.TipoAlojamentoFilter;
import utils.Utils;

public class ListarTipoAlojamentoUI {

    private Organizacao m_oOrganizacao;
    private ListarTipoAlojamentoController m_controller;

    public ListarTipoAlojamentoUI(Organizacao oOrganizacao)
    {
        this.m_oOrganizacao = oOrganizacao;
        m_controller = new ListarTipoAlojamentoController(m_oOrganizacao);
    }

    public void run()
    {
        System.out.println("\nListar Tipos de Alojamento:");

        TipoAlojamentoFilter tipoAlojamentoFilter = readTipoAlojamentoFilter();

        String sDeFiltro = Utils.readLineFromConsole("String de filtro: ");

        List<TipoAlojamento> listaTipoAlojamento = m_controller.filtrar(tipoAlojamentoFilter, sDeFiltro);

        System.out.println("\nLista de Tipo de Alojamento filtrada:\n");
        for (TipoAlojamento ta : listaTipoAlojamento) {
            System.out.println(ta.toString() + "\n");
        }

        Utils.readLineFromConsole("Continuar?");
    }

    private TipoAlojamentoFilter readTipoAlojamentoFilter() 
    {
        List<TipoAlojamentoFilter> lstTipoAlojamentoFilters = m_controller.getTipoAlojamentoFilters();

        int nIndex = 0;
        do {
            int i = 0;
            for (TipoAlojamentoFilter tipoAlojamentoFilter : lstTipoAlojamentoFilters) {
                i++;
                System.out.println(i + ". " + tipoAlojamentoFilter.getName() );
            }

            String sTipoAlojamentoFilter = Utils.readLineFromConsole("Tipo Alojamento Filter: ");

            nIndex = Integer.parseInt(sTipoAlojamentoFilter );

            if( nIndex <= lstTipoAlojamentoFilters.size() )
                return lstTipoAlojamentoFilters.get( nIndex - 1 );
        }
        while ( nIndex !=0 );

        return null;
    }
}

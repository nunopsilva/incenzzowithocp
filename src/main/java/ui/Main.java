package ui;

import java.io.File;
import java.util.Arrays;

import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.builder.fluent.Configurations;
import org.apache.commons.configuration2.ex.ConfigurationException;

import domain.FactoryAlojamento;
import domain.FactoryTipoAlojamento;
import domain.ImplFactoryAlojamento;
import domain.ImplFactoryTipoAlojamento;
import domain.Organizacao;

//Teste
public class Main {

        public static void main(String[] args)
        {
            String[] listStringClassesTipoAlojamentoFilters = {};
            String[] listStringClassesAlojamentoFiltersByDenominacao = {};
            String[] listStringClassesAlojamentoFiltersByIntervaloQtd = {};

            String[] listStringUI = {};

            Configurations configs = new Configurations();
            try
            {
                Configuration config = configs.properties(new File("config.properties"));

                // access configuration properties
                listStringClassesTipoAlojamentoFilters = config.getStringArray("filters.tipoalojamento");

                listStringClassesAlojamentoFiltersByDenominacao = config.getStringArray("filters.alojamentoByDenominacao");
                listStringClassesAlojamentoFiltersByIntervaloQtd = config.getStringArray("filters.alojamentoByIntervaloQtd");

                listStringUI = config.getStringArray("ui");

                try
                {
                    FactoryTipoAlojamento factoryTipoAlojamento = new ImplFactoryTipoAlojamento();
                    FactoryAlojamento factoryAlojamento = new ImplFactoryAlojamento();
    
                    Organizacao organizacao = new Organizacao( factoryTipoAlojamento, factoryAlojamento,
                                                                Arrays.asList(listStringClassesTipoAlojamentoFilters),
                                                                Arrays.asList(listStringClassesAlojamentoFiltersByDenominacao),
                                                                Arrays.asList(listStringClassesAlojamentoFiltersByIntervaloQtd) );
    
                    MenuUI uiMenu = new MenuUI(organizacao);
    
                    uiMenu.run( Arrays.asList(listStringUI) );
                }
                catch( Exception e )
                {
                    e.printStackTrace();
                }    
            }
            catch (ConfigurationException exception)
            {
                // Something went wrong
                System.out.println("Algo correu mal na leitura de configuração: " + exception.getMessage());
            }
        }
}

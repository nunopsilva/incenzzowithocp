package ui;

import domain.Organizacao;
import utils.Utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MenuUI {

        private Organizacao m_organizacao;

        public MenuUI(Organizacao organizacao)
        {
            m_organizacao = organizacao;
        }

        public void run( List<String> listStringClassUI ) throws IOException
        {

            ArrayList<UI> arrayClassesUI = carregaClassesUI( listStringClassUI );

            int opcao = 0;
            do
            {              
                opcao = readOpcaoMenu(arrayClassesUI);

                if( opcao != 0) {
                    UI uiOpcao = arrayClassesUI.get( opcao - 1 );

                    uiOpcao.run();
                }
            }
            while (opcao != 0 );
        }

        private int readOpcaoMenu( ArrayList<UI> arrayClassesUI ) {

            System.out.println("\n\n");

            int i = 1;
            for (UI ui : arrayClassesUI ) {

              System.out.println(i + ". " + ui.getMenuDescription() );
              i++;
            }

            System.out.println("0. Sair");


            String opcaoStr = Utils.readLineFromConsole("Introduza opção: ");
            
            return Integer.parseInt(opcaoStr);
        }

        private ArrayList<UI> carregaClassesUI( List<String> listStringClassUI ) {

            ArrayList<UI> lClassesUI = new ArrayList<UI>();
    
            for (String strClassUI : listStringClassUI) {
                try {
                    UI ui = (UI) Class.forName(strClassUI).getDeclaredConstructor(Organizacao.class).newInstance(this.m_organizacao);
                    lClassesUI.add(ui);
                }
                catch(Exception e) {
                    System.out.println( e.getMessage() );
                }
            }
    
            return lClassesUI;
        }
    
}

package ui;

public interface UI {
    public void run();

    public String getMenuDescription();
}

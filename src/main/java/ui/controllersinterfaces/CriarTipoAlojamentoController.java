package ui.controllersinterfaces;

public interface CriarTipoAlojamentoController {
    
    boolean guardaTipoAlojamento();
    boolean criaTipoAlojamento(String sDescricao);
    String getTipoAlojamentoString();
}

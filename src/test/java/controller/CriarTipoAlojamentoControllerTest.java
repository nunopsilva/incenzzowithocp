package controller;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;

import org.junit.Test;

import domain.FactoryAlojamento;
import domain.FactoryTipoAlojamento;
import domain.Organizacao;
import domain.TipoAlojamento;

/**
 * Unit test for CriarTipoAlojamentoController.
 */
public class CriarTipoAlojamentoControllerTest {

    @Test
    public void shouldCreateAValidCriarTipoAlojamentoController() throws Exception
    {
        Organizacao oOrganizacaoDouble = mock(Organizacao.class);

        new CriarTipoAlojamentoController1( oOrganizacaoDouble );
    }

    @Test
    public void shouldThrowExceptionWithEmptyOrganizacao_inJUnit5() throws Exception
    {
        Exception exception = assertThrows(Exception.class, () -> {
            new CriarTipoAlojamentoController1( null );
        });
    
        String expectedMessage = "Organizacao não pode ser null.";
        String actualMessage = exception.getMessage();
    
        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    public void shouldThrowExceptionWithEmptyFactoryAlojamento_inJUnit5() throws Exception
    {
        FactoryTipoAlojamento factoryTipoAlojamentoDouble = mock( FactoryTipoAlojamento.class);

        Exception exception = assertThrows(Exception.class, () -> {
            new Organizacao(factoryTipoAlojamentoDouble, null, new ArrayList<>(), new ArrayList<>(), new ArrayList<>());
        });
    
        String expectedMessage = "FactoryAlojamento não pode ser null.";
        String actualMessage = exception.getMessage();
    
        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    public void shouldThrowExceptionWithEmptyFactoryTipoAlojamento_inJUnit5() throws Exception
    {
        FactoryAlojamento factoryAlojamentoDouble = mock( FactoryAlojamento.class);

        Exception exception = assertThrows(Exception.class, () -> {
            new Organizacao(null, factoryAlojamentoDouble, new ArrayList<>(), new ArrayList<>(), new ArrayList<>());
        });
    
        String expectedMessage = "FactoryTipoAlojamento não pode ser null.";
        String actualMessage = exception.getMessage();
    
        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    public void shouldCreateAValidTipoAlojamento()
    {
        // Arrange
        Organizacao oOrganizacao = mock(Organizacao.class);
        TipoAlojamento oTipoAlojamentoDouble = mock( TipoAlojamento.class );
        
        when( oOrganizacao.novoTipoAlojamento("desc")).thenReturn( oTipoAlojamentoDouble );
        when( oOrganizacao.validaTipoAlojamento(oTipoAlojamentoDouble)).thenReturn( true );

        // Act
        CriarTipoAlojamentoController1 oController = new CriarTipoAlojamentoController1( oOrganizacao );
        boolean bResult = oController.criaTipoAlojamento("desc");
     
        // Assert
        assertTrue( bResult );
    }

    @Test
    public void shouldNotCreateATipoAlojamentoWithZeroedDescription()
    {
        // Arrange
        Organizacao oOrganizacao = mock(Organizacao.class);
        when( oOrganizacao.novoTipoAlojamento("")).thenThrow(IllegalArgumentException.class); // !!!

        // Act
        CriarTipoAlojamentoController1 oController = new CriarTipoAlojamentoController1( oOrganizacao );
        boolean bResult = oController.criaTipoAlojamento("");
     
        // Assert
        assertFalse( bResult );
    }

    /*
    /
    / falta os restantes métodos e completar os testes aos métodos anteriores
    /
    */
}

package domain;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;

import org.junit.Test;

/**
 * Unit test for Alojamento.
 */
public class AlojamentoTest {


    /*
    /
    / este teste não isola Alojamento de TipoAlojamento
    / pelo que, se considerarmos a class Alojamento como unidade, este teste não é um teste unitário
    /
    */
    @Test
    public void shouldCreateAValidAlojamentoWithoutIsolation() throws Exception
    {
        TipoAlojamento tipoAlojamento = new TipoAlojamento("Hotel");

        new Alojamento("Hotel1, ", tipoAlojamento, Alojamento.DiaSemana.DOMINGO, 1, 2, 10);
    }

    @Test
    public void shouldCreateAValidAlojamento() throws Exception
    {
        TipoAlojamento tipoAlojamentoDouble = mock( TipoAlojamento.class);

        new Alojamento("Hotel1, ", tipoAlojamentoDouble, Alojamento.DiaSemana.DOMINGO, 1, 2, 10);
    }

    /*
    /
    / outros testes ao construtor
    /
    */

    @Test
    public void shouldReturnTrueEqualsWithSameObject()
    {
        TipoAlojamento tipoAlojamentoDouble = mock( TipoAlojamento.class);

        Alojamento alojamento = new Alojamento("Hotel1, ", tipoAlojamentoDouble, Alojamento.DiaSemana.DOMINGO, 1, 2, 10);

        boolean isEquals = alojamento.equals(alojamento);

        assertTrue( isEquals );
    }

    @Test
    public void shouldReturnTrueEqualsAlojamentosWithSameParameters()
    {
        TipoAlojamento tipoAlojamentoDouble = mock( TipoAlojamento.class);

        Alojamento alojamento = new Alojamento("Hotel1, ", tipoAlojamentoDouble, Alojamento.DiaSemana.DOMINGO, 1, 2, 10);

        Alojamento alojamento2 = new Alojamento("Hotel1, ", tipoAlojamentoDouble, Alojamento.DiaSemana.DOMINGO, 1, 2, 10);


        boolean isEquals = alojamento.equals(alojamento2);

        assertTrue( isEquals );
    }

    @Test
    public void shouldReturnFalseEqualsAlojamentosWithDifDescriptions() {
        TipoAlojamento tipoAlojamentoDouble = mock( TipoAlojamento.class);

        Alojamento alojamento = new Alojamento("Hotel1, ", tipoAlojamentoDouble, Alojamento.DiaSemana.DOMINGO, 1, 2, 10);

        Alojamento alojamento2 = new Alojamento("Hotel2, ", tipoAlojamentoDouble, Alojamento.DiaSemana.DOMINGO, 1, 2, 10);


        boolean isEquals = alojamento.equals(alojamento2);

        assertFalse( isEquals );
    }

    @Test
    public void shouldReturnFalseDifferentClasses() {
        TipoAlojamento tipoAlojamentoDouble = mock( TipoAlojamento.class);

        Alojamento alojamento = new Alojamento("Hotel1, ", tipoAlojamentoDouble, Alojamento.DiaSemana.DOMINGO, 1, 2, 10);

        Object obj = new Object();

        boolean isEquals = alojamento.equals(obj);

        assertFalse( isEquals );
    }

    /*
    /
    / outros testes ao equals
    /
    */
}

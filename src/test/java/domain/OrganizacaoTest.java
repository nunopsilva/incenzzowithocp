package domain;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

//import domain.filters.AlojamentoFilterByDenominacao;

import domain.filters.TipoAlojamentoFilter;

/**
 * Unit test for Organizacao.
 */
public class OrganizacaoTest {

    @Test
    public void shouldCreateAValidOrganizacao() throws Exception
    {
        FactoryTipoAlojamento factoryTipoAlojamentoDouble = mock( FactoryTipoAlojamento.class);
        FactoryAlojamento factoryAlojamentoDouble = mock( FactoryAlojamento.class);

        //new Organizacao( factoryTipoAlojamentoDouble, factoryAlojamentoDouble, new ArrayList<String>() );
    }


    @Test
    public void shouldThrowExceptionWithEmptyFactoryTipoAlojamento_inJUnit5() throws Exception
    {
        Exception exception = assertThrows(Exception.class, () -> {
            new Organizacao(null, null, new ArrayList<>(), new ArrayList<>(), new ArrayList<>() );
        });
    
        String expectedMessage = "FactoryTipoAlojamento não pode ser null.";
        String actualMessage = exception.getMessage();
    
        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    public void shouldThrowExceptionWithEmptyFactoryAlojamento_inJUnit5() throws Exception
    {
        FactoryTipoAlojamento factoryTipoAlojamentoDouble = mock( FactoryTipoAlojamento.class);

        Exception exception = assertThrows(Exception.class, () -> {
            new Organizacao(factoryTipoAlojamentoDouble, null, new ArrayList<>(), new ArrayList<>(), new ArrayList<>());
        });
    
        String expectedMessage = "FactoryAlojamento não pode ser null.";
        String actualMessage = exception.getMessage();
    
        assertTrue(actualMessage.contains(expectedMessage));
    }


    @Test
    public void shouldReturnANewTipoAlojamento()
    {
        // Arrange
        FactoryTipoAlojamento factoryTipoAlojamentoDouble = mock( FactoryTipoAlojamento.class);
        FactoryAlojamento factoryAlojamentoDouble = mock( FactoryAlojamento.class);

        //Organizacao organizacao = new Organizacao( factoryTipoAlojamentoDouble, factoryAlojamentoDouble, new ArrayList<String>() );

        TipoAlojamento oTipoAlojamentoDouble = mock( TipoAlojamento.class );
        when( factoryTipoAlojamentoDouble.criarTipoAlojamento( "desc" )).thenReturn( oTipoAlojamentoDouble );

        // Act
        //TipoAlojamento oTipoAlojamento = organizacao.novoTipoAlojamento("desc");

        // Assert
        //assertEquals( oTipoAlojamento, oTipoAlojamentoDouble );
    }

/*
    @Test
    public void shouldReturnANonEmptyListOfTipoAlojamento()
    {

        List<String> lStrings = mock(List.class);

        // Arrange
        FactoryTipoAlojamento oFactoryTipoAlojamento = mock(FactoryTipoAlojamento.class);
        FactoryAlojamento oFactoryAlojamento = mock(FactoryAlojamento.class);
        
        List<String> listaStringClassesTipoAlojamentoFilters = new ArrayList<>();
        List<String> listStringClassesAlojamentoFiltersByDenominacao = new ArrayList<>();
        List<String> listStringClassesAlojamentoFiltersByIntervaloQtd = new ArrayList<>();

        Organizacao organizacao = new Organizacao(
                                            oFactoryTipoAlojamento,
                                            oFactoryAlojamento,
                                            listaStringClassesTipoAlojamentoFilters,
                                            listStringClassesAlojamentoFiltersByDenominacao,
                                            listStringClassesAlojamentoFiltersByIntervaloQtd);

        Alojamento oAlojamento1 = mock(Alojamento.class);
        organizacao.guardaAlojamento(oAlojamento1);
        
        Alojamento oAlojamento2 = mock(Alojamento.class);
        organizacao.guardaAlojamento(oAlojamento2);

        AlojamentoFilterByDenominacao filtro = mock(AlojamentoFilterByDenominacao.class);
        when(filtro.complies(oAlojamento1, "h")).thenReturn(true);
        when(filtro.complies(oAlojamento2, "h")).thenReturn(false);

        List<Alojamento> listaExpectavel = new ArrayList<>();
        listaExpectavel.add( oAlojamento1 );

        // Act
        List<Alojamento> listaFiltrada = organizacao.filtrarAlojamentosByDenominacao(filtro, "h");

        // Assert
        //assertEquals(listaFiltrada, listaExpectavel);
        assertArrayEquals(listaFiltrada.toArray(), listaExpectavel.toArray());
    }
*/

    public void shouldReturnListWithOneElement() {

        // Arrange
        TipoAlojamentoFilter filtro = mock(TipoAlojamentoFilter.class);

        FactoryTipoAlojamento oFactoryTipoAlojamento = mock(FactoryTipoAlojamento.class);
        FactoryAlojamento oFactoryAlojamento = mock(FactoryAlojamento.class);
        
        List<String> listaStringClassesTipoAlojamentoFilters = new ArrayList<>();
        List<String> listStringClassesAlojamentoFiltersByDenominacao = new ArrayList<>();
        List<String> listStringClassesAlojamentoFiltersByIntervaloQtd = new ArrayList<>();

        Organizacao organizacao = new Organizacao(
            oFactoryTipoAlojamento,
            oFactoryAlojamento,
            listaStringClassesTipoAlojamentoFilters,
            listStringClassesAlojamentoFiltersByDenominacao,
            listStringClassesAlojamentoFiltersByIntervaloQtd);

        TipoAlojamento oTipoAlojamento1 = mock(TipoAlojamento.class);
        //when(oTipoAlojamento1.getDescricao()).thenReturn("Hotel");

        TipoAlojamento oTipoAlojamento2 = mock(TipoAlojamento.class);
        //when(oTipoAlojamento2.getDescricao()).thenReturn("Pensão");

        organizacao.guardaTipoAlojamento(oTipoAlojamento1);
        organizacao.guardaTipoAlojamento(oTipoAlojamento2);

        when(filtro.complies(oTipoAlojamento1, "Ho")).thenReturn(true);
        when(filtro.complies(oTipoAlojamento2, "Ho")).thenReturn(false);

        List<TipoAlojamento> listaExpectavel = new ArrayList<>();
        listaExpectavel.add(oTipoAlojamento1);

        // Act
        List<TipoAlojamento> listaFiltrada =
                    organizacao.filtrarTiposAlojamento( filtro, "Ho" );

        // Assert
        //assertEquals(listaFiltrada, listaExpectavel);
        assertArrayEquals(listaExpectavel.toArray(), listaFiltrada.toArray());
    }


}

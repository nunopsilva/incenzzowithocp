package domain;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;

import org.junit.Test;

/**
 * Unit test for TipoAlojamento.
 */
public class TipoAlojamentoTest {
    @Test
    public void shouldCreateAValidTipoAlojamento() throws Exception
    {
        new TipoAlojamento("Hotel");
    }

    @Test(expected = Exception.class)
    public void shouldThrowExceptionWithEmptyName_inJUnit4() throws Exception
    {
        new TipoAlojamento("");
    }

    @Test(expected = Exception.class)
    public void shouldThrowExceptionWithSingleSpaceName_inJUnit4() throws Exception
    {
        new TipoAlojamento(" ");
    }

    @Test(expected = Exception.class)
    public void shouldThrowExceptionWithMultipleSpacesName_inJUnit4() throws Exception
    {
        new TipoAlojamento("   ");
    }

    @Test(expected = Exception.class)
    public void shouldThrowExceptionWithTabName_inJUnit4() throws Exception
    {
        new TipoAlojamento("\t");
    }

    @Test
    public void shouldThrowExceptionWithEmptyName_inJUnit5() throws Exception
    {
        Exception exception = assertThrows(Exception.class, () -> {
            new TipoAlojamento("");
        });
    
        String expectedMessage = "Valor de parâmetro 'descrição' deve ser uma string não vazia.";
        String actualMessage = exception.getMessage();
    
        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    public void shouldReturnTrueEqualsWithSameObject() {
        TipoAlojamento hotel = new TipoAlojamento("Hotel");

        boolean isEquals = hotel.equals(hotel);

        assertTrue( isEquals );
    }

    @Test
    public void shouldReturnTrueEqualsTipoAlojamentosWithSameDescription() {
        TipoAlojamento hotel = new TipoAlojamento("Hotel");

        TipoAlojamento hotel2 = new TipoAlojamento("Hotel");

        boolean isEquals = hotel.equals(hotel2);

        assertTrue( isEquals );
    }

    @Test
    public void shouldReturnFalseEqualsTipoAlojamentosWithDifDescriptions() {
        TipoAlojamento hotel = new TipoAlojamento("Hotel");

        TipoAlojamento hotel2 = new TipoAlojamento("Hotel2");

        boolean isEquals = hotel.equals(hotel2);

        assertFalse( isEquals );
    }

    @Test
    public void shouldReturnFalseDifferentClasses() {
        TipoAlojamento hotel = new TipoAlojamento("Hotel");

        Object obj = new Object();

        boolean isEquals = hotel.equals(obj);

        assertFalse( isEquals );
    }

    @Test
    public void shouldReturnFalse_becauseTipoAlojamentoDoubleHasNullDescription() {
        TipoAlojamento hotel = new TipoAlojamento("Hotel");

        TipoAlojamento tipoAlojamentoDouble = mock(TipoAlojamento.class);

        boolean isEquals = hotel.equals(tipoAlojamentoDouble);

        assertFalse( isEquals );
    }

    /*
    // este teste falha porque não é possível substituir equals()

    @Test
    public void shouldReturnFalse_becauseTipoAlojamentoDoubleEqualsIsNotStubed() {
        TipoAlojamento hotel = new TipoAlojamento("Hotel");

        TipoAlojamento tipoAlojamentoDouble = mock(TipoAlojamento.class);
        when( tipoAlojamentoDouble.equals( hotel )).thenReturn(true);

        boolean isEquals = tipoAlojamentoDouble.equals(hotel);

        assertTrue( isEquals );
    }
    */
}

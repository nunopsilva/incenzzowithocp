package domain.filters.tipoalojamento;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import domain.TipoAlojamento;
import domain.filters.TipoAlojamentoFilter;

public class TipoAlojamentoFilterIncludesTest {

    static TipoAlojamentoFilter filtro;
    static TipoAlojamento ta;

    @BeforeAll
    static public void setup() {
        // Arrange
        TipoAlojamentoFilterIncludesTest.filtro = new TipoAlojamentoFilterIncludes();
        
        ta = mock(TipoAlojamento.class);

        String strDesc = ta.getDescricao();  // null!!!
        when(ta.getDescricao()).thenReturn("Hotel3*HoteL5+");
        String strDesc2 = ta.getDescricao();  // not null!!!
    }

    @Test
    public void shouldReturnTrueWithDescriptionHotelAndStringHotel() {
        // Act
        boolean bResult = filtro.complies( ta, "Hotel" );
        // Assert
        assertTrue(bResult);
    }

    @Test
    public void shouldReturnFalseWithDescriptionHotelAndStringh() {

        // Act
        boolean bResult = filtro.complies( ta, "h" );

        // Assert
        assertFalse(bResult);
    }

    @Test
    public void shouldReturnTrueWithDescriptionHotelAndStringot() {

        // Act
        boolean bResult = filtro.complies( ta, "ot" );

        // Assert
        assertTrue(bResult);
    }
}
